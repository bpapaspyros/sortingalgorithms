Sorting Algorithms
==================

Templated implementation of basic sorting algorithms

Already implemented:

  - Merge Sort
  - Quick Sort
  - Buble Sort

Under development:

  - Insertion Sort
  - Heap Sort
  - Counting Sort
  - Radix Sort

> At the moment there is a cpp file for testing purposes. I am planning to make a sample main function so that you will be able to test the projects features with sample inputs.

The purpose of this project is to create a templated library of sorting algoruthms for use in multiple other projects.

Maybe I will extend the project with more templated implementations in the future.



Compiling and Running
---------------------

```sh
mkdir build && cs build
cmake .. && make
cd ../bin
./test
```
