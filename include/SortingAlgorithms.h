/** 
 * @class SortingAlgorithms
 *														    
 * @brief Templated implementation of basic sorting algorithms 									    
 * 																			    
 * The purpose of this class is to implement basic sorting algorithms
 * with the use of templates to achieve a generic class that will be
 * used in numerous applications that need to use sorting algorithms																    
 *																				    
 * @author Vaios Papaspyros							            
 *																				    
 * Contact: b.papaspyros@gmail.com
 *																				   
 */

#ifndef SORT_N_SEARCH_H
#define SORT_N_SEARCH_H

#include <cmath>
#include <ctime>

class SortingAlgorithms {
	public:
		/**
		 * @brief Mergesort templated implementation
		 * @param A T array and its lower and upper limit
		 */
		template <class T>
		void mergesort(T* inArray, int lower_lim, int upper_lim);
		
		/**
		 * @brief Quicksort templated implementation
		 * @param A T array and its lower and upper limit
		 */
		template <class T>
		void quicksort(T* inArray, int lower_lim, int upper_lim);

		/**
		 * @brief Bublesort templated implementation
		 * @param A T array and its size
		 */
		template <class T>
		void bublesort(T* inArray, int size);

		/**
		 * @brief Getter for the algorithm execution time
		 * @return Double that stands for the execution time (seconds)
		 */
		double getTime();
	private:
		/**
		 * @brief Merges two parts of the array together into 
		 * one sorted
		 * @param A T array and its lower and upper limit
		 */
		template <class T>
		void merge(T* inArray, int lower_lim, int upper_lim);
		


		/**
		 * @brief Templated swap method
		 * @param Two T values to swaps
		 */
		template <class T>
		void swap(T& a, T& b);

		/**
		 * @brief Gets the system time
		 */
		void start_timer();

		/**
		 * @brief Gets the system time and calculates 
		 * the difference from the time previously taken
		 */
		void stop_timer();

		clock_t t_start;	// system time when the algorithm started 
		double dtime;		// time spent on the algorithm
};






/*- -------------------------------------------------------------- -*/
template <class T>
void SortingAlgorithms::mergesort(T* inArray, int lower_lim, int upper_lim) {
	if (lower_lim < upper_lim){
		mergesort(inArray, lower_lim, (lower_lim+upper_lim)/2);
		mergesort(inArray, (lower_lim+upper_lim)/2+1, upper_lim);
		merge(inArray, lower_lim, upper_lim);
	}
}

template <class T>
void SortingAlgorithms::merge(T* inArray, int lower_lim, int upper_lim) {
    int mid = floor((lower_lim + upper_lim) / 2);
    int i1 = 0;
    int i2 = lower_lim;
    int i3 = mid + 1;

    // storing the changes temporarily in a buffer
    T temp[upper_lim-lower_lim+1];

    // merge in sorted form the 2 arrays
    while ( i2 <= mid && i3 <= upper_lim ) {
        if ( inArray[i2] < inArray[i3] )
            temp[i1++] = inArray[i2++];
        else
            temp[i1++] = inArray[i3++];
    }

    // merge the remaining elements in the left array
    while ( i2 <= mid ) {
        temp[i1++] = inArray[i2++];
    }

    // merge the remaining elements in the right array
    while ( i3 <= upper_lim ) {
        temp[i1++] = inArray[i3++];
    }

    // transfer the changes to the given array
    for ( int i = lower_lim; i <= upper_lim; i++ ) {
        inArray[i] = temp[i-lower_lim];
    }
}

/*- -------------------------------------------------------------- -*/
template <class T>
void SortingAlgorithms::quicksort(T* inArray, int lower_lim, int upper_lim) {
	// left and right limits
	int i = lower_lim;
	int k = upper_lim+1;

	// setting the pivot
	T s = inArray[lower_lim];

	while (i<k) {
		do{
			i++;
		}while(inArray[i]<s);

		do{
			k--;
		}while(inArray[k]>s);

		if (k>i) {
			swap(inArray[k], inArray[i]);
		}
	}

	swap(inArray[lower_lim], inArray[k]);

	if (lower_lim<k-1) {
		quicksort(inArray, lower_lim, k-1);
	}

	if (k+1<upper_lim) {
		quicksort(inArray, k+1, upper_lim);
	}
}

/*- -------------------------------------------------------------- -*/
template <class T>
void SortingAlgorithms::bublesort(T* inArray, int size) {
	while (size>1) {
		int j = 0;
		for (int i=0; i<size-1; i++) {
			if (inArray[i]>inArray[i+1]) {
				swap(inArray[i], inArray[i+1]);
				j++;
			}
		}

		if (j == 0) {
			break;
		}

		size--;
	}
}




/*- -------------------------------------------------------------- -*/
template <class T>
void SortingAlgorithms::swap(T& a, T& b) {
	T temp = a;
	a = b;
	b = temp;
}

/*- -------------------------------------------------------------- -*/
void SortingAlgorithms::start_timer() {
	t_start = clock();
}

/*- -------------------------------------------------------------- -*/
void SortingAlgorithms::stop_timer() {
	dtime =  (double)(clock() - t_start)/CLOCKS_PER_SEC;
}

/*- -------------------------------------------------------------- -*/
double SortingAlgorithms::getTime() {
	return dtime;
}

#endif